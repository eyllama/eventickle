
const express = require('express');
const app = express();

const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/app/backend'));

const PORT = process.env.PORT || 3000;

app.get('/', (req, res) => {
  res.json({ message: 'API init, happy coding!' });
});

require('./app/backend/App')(app);

app.listen(PORT, (req, res) => console.log('Eventickle server is running...slowly...'));
