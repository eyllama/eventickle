-- Setup file for Eventickle DB
-- September 2018

/* - - - TYPES - - - */

-- DO $$
-- BEGIN
--     IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typename = 'FRIENDSHIP_STATUS') THEN
--         CREATE TYPE FRIENDSHIP_STATUS AS (
--             'Pending',
--             'Accepted',
--             'Declined',
--             'Blocked'
--         );
--     END IF;
-- END$$;

/* - - - TABLES - - - */

CREATE TABLE IF NOT EXISTS "User" (
	uid SERIAL PRIMARY KEY,
	email VARCHAR(250) NOT NULL,
	username VARCHAR(50) UNIQUE NOT NULL,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	profile_picture VARCHAR(250) NOT NULL,
	password VARCHAR(250) NOT NULL,
  UNIQUE(email)
);

CREATE TABLE IF NOT EXISTS Friendship (
    uid_one INTEGER REFERENCES "User"(uid),
    uid_two INTEGER REFERENCES "User"(uid),
    status INTEGER NOT NULL,
    action_uid INTEGER NOT NULL,
    PRIMARY KEY(uid_one, uid_two),
    CHECK(status = 0 OR status = 1 OR status = 2 OR status = 3)
);

CREATE TABLE IF NOT EXISTS Address (
    aid SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    street VARCHAR(200) NOT NULL,
    zipcode VARCHAR(25),
    coords POINT NOT NULL
);

CREATE TABLE IF NOT EXISTS User_Address (
    aid INTEGER REFERENCES Address(aid),
    uid INTEGER REFERENCES "User"(uid),
    PRIMARY KEY(aid, uid)
);

CREATE TABLE IF NOT EXISTS "Event" (
    eid SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    host_id INTEGER REFERENCES "User"(uid),
    aid INTEGER REFERENCES Address(aid),
    start_time TIMESTAMP NOT NULL,
		cover_photo VARCHAR(250) NOT NULL,
		date_occurring TIMESTAMP NOT NULL,
    date_created TIMESTAMP NOT NULL
		description VARCHAR(250),
);

CREATE TABLE IF NOT EXISTS User_Event (
		uid INTEGER REFERENCES "User"(uid),
    eid INTEGER REFERENCES "Event"(eid),
    attendance_status VARCHAR(10) NOT NULL DEFAULT 'RSVP',
    PRIMARY KEY(uid, eid),
    CHECK(attendance_status = 'RSPV' OR attendance_status = 'Going' OR attendance_status = 'Maybe' OR attendance_status = 'Cant')
);

CREATE TABLE IF NOT EXISTS Event_Album (
    eid INTEGER REFERENCES "Event"(eid),
    uid INTEGER REFERENCES "User"(uid),
    photo_url VARCHAR(250) NOT NULL,
    date_created TIMESTAMP NOT NULL,
    PRIMARY KEY(eid, uid, photo_url)
);

CREATE TABLE IF NOT EXISTS Post (
    pid SERIAL PRIMARY KEY,
	  eid INTEGER REFERENCES "Event"(eid),
    uid INTEGER REFERENCES "User"(uid),
    type VARCHAR(15),
    comment VARCHAR(250),
		reaction_icon TEXT,
    reaction_label VARCHAR(25),
    album_new_count INTEGER,
    date_created TIMESTAMP NOT NULL,
    CHECK(type = 'reaction' OR type = 'album_update' OR type = 'regular')
);

/* - - - INSERT STATEMENTS - - - */

/* User */

INSERT INTO "User" (email, username, first_name, last_name, profile_picture, password) VALUES (
	'eylam.fried96@gmail.com',
	'eyllama',
	'Eylam',
	'Fried',
	'profile_picture placeholder',
	'password'
);
INSERT INTO "User" (email, username, first_name, last_name, profile_picture, password) VALUES (
	'sanjana.pandit@gmail.com',
	'sanjy',
	'Sanjana',
	'Pandit',
	'profile_picture placeholder',
	'password'
);
INSERT INTO "User" (email, username, first_name, last_name, profile_picture, password) VALUES (
	'katie.winters@gmail.com',
	'kwints',
	'Katie',
	'Winters',
	'profile_picture placeholder',
	'password'
);
INSERT INTO "User" (email, username, first_name, last_name, profile_picture, password) VALUES (
	'steven.garverick@gmail.com',
	'swgarv',
	'Steven',
	'Garverick',
	'profile_picture placeholder',
	'password'
);
INSERT INTO "User" (email, username, first_name, last_name, profile_picture, password) VALUES (
	'remy.fujioka@gmail.com',
	'mt_fuji',
	'Remy',
	'Fujioka',
	'profile_picture placeholder',
	'password'
);

/* Event */
INSERT INTO "Event" (name, cover_photo, host_id, aid, start_time, date_created, date_occurring) VALUES (
	'New Years Party!',
	'https://cdns.abclocal.go.com/content/creativecontent/images/cms/1675493_1280x720.jpg',
	1,
	1,
	NOW(),
	NOW(),
	NOW()
);

/* User_Event */

INSERT INTO User_Event VALUES (2, 1, 'Going');

/* Friendship */

INSERT INTO Friendship VALUES (1, 2, 1, 1);
INSERT INTO Friendship VALUES (1, 3, 1, 3);
INSERT INTO Friendship VALUES (1, 5, 0, 5);

/* Address */

INSERT INTO Address (name, street, zipcode, coords) VALUES (
    'My house',
    '672 Meadow Ln.',
    '94022',
    point(37.394299,-122.126713)
);

/* Post */


/**/
