// @flow

import { Navigation } from 'react-native-navigation';

import React from 'react';
import type { Element } from 'react';

import ErrorModal from '../components/modals/ErrorModal';

class GlobalController {

  _animationTypes: Object;

  constructor() {
    this._animationTypes = {
      slide: 'SlideModal',
      popup: 'PopupModal',
    };
  }

  showModal(type: string, component: Element<*>, height?: number = 334) {
    Navigation.showOverlay({
      component: {
        name: this._animationTypes[type],
        passProps: { child: component, height },
        options: {
          overlay: { interceptTouchOutside: true }
        },
      },
    });
  }

  hideModal(componentId: string) {
    Navigation.dismissOverlay(componentId);
  }

  showErrorModal(props?: Object) {
    const component = (
      <ErrorModal {...props} />
    );

    this.showModal('popup', component);
  }
}

export default new GlobalController();