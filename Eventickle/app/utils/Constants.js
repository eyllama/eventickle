// @flow

import { Easing } from 'react-native';

type EmojiReactionType = {
  icon: string,
  label: string,
};

class Constants {

  get localUrl(): string {
    return 'http://192.168.1.189:3000/api';
  }

  get simpleEaseIn() {
    return Easing.bezier(0.165, 0.84, 0.44, 1);
  }

  get simpleEaseOut() {
    return Easing.out(Easing.poly(4));
  }

  get simpleElasticEase() {
    return Easing.elastic(1.1);
  }

  get simpleHeaders(): Object {
    return {
      'Content-Type': 'application/json',
    };
  }

  get emojiReactions(): Array<EmojiReactionType> {
    return [
      { icon: '😁', label: 'excited' },
      { icon: '💪', label: 'pumped' },
      { icon: '🔥', label: 'fired up' },
      { icon: '🎮', label: 'game' },
      { icon: '🍎', label: 'juiced' },
    ];
  }

}

export default new Constants();
