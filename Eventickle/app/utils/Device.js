// @flow

import { Platform, Dimensions, StatusBar } from 'react-native';
import { Navigation } from 'react-native-navigation';

class Device {

  navigationConstants: Object;

  async _getNavigationConstants() {
    const constants = await Navigation.constants();
    return constants;
  }

  get screenWidth(): number {
    return Dimensions.get('window').width;
  }

  get screenHeight(): number {
    return Dimensions.get('window').height;
  }

  get isIPhoneX(): boolean {
    return Platform.OS === 'ios' && (this.screenWidth == 812 || this.screenHeight == 812);
  }

  get statusBarHeight(): number {
    return (this.isIPhoneX) ? 45 : (Platform.OS === 'ios' ? 21 : StatusBar.currentHeight);
  }

  get topBarHeight(): number {
    return this.navigationConstants.topBarHeight;
  }

}

export default new Device();