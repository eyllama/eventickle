// @flow

import * as Types from './Types';
import Constants from '../../utils/Constants';
import FeedPost from '../../models/FeedPost';
import GlobalController from '../../utils/GlobalController';

export const fetchFeedForEvent = (eventID: number) => {
  const url = `${Constants.localUrl}/event/${eventID}/feed`;
  
  return function(dispatch: (any) => void) {
    return fetch(url)
    .then((json: Object) => json.json())
    .then((feed: Array<Object>) => {
      const result = [];
      feed.forEach(post => {
        result.push(new FeedPost(post));
      });

      dispatch(emitFeed(result));
    })
    .catch((error: Object) => {
      console.warn('Error getting feed: (fetchFeedForEvent, FeedActions.js)', error);
      GlobalController.showErrorModal({ message: 'Fetching feed' });
    });
  }
};

const emitFeed = (feed: Array<FeedPost>) => {
  return {
    type: Types.SET_FEED,
    feed,
  };
};

export const createPost = (post: FeedPost) => {
  const url = `${Constants.localUrl}/event/feed`;
  
  return function(dispatch: (any) => void) {
    return fetch(url, {
      method: 'POST',
      headers: Constants.simpleHeaders,
      body: JSON.stringify(post),
    })
    .then((response: Object) => {
      if (!response.ok) {
        throw new Error();
      } else {
        return response.json();
      }
    })
    .then((newPost: Object) => {
      console.log('newPost', newPost);
      dispatch(emitPost(new FeedPost(newPost)));
    })
    .catch((error: Object) => {
      console.log('Error in createPost - FeedActions.js', error);
      GlobalController.showErrorModal();
    });
  }
};

const emitPost = (post: FeedPost) => {
  return {
    type: Types.ADD_POST,
    post,
  };
};