// @flow

/* events */
export const SET_EVENTS = 'SET_EVENTS';

/* feed */
export const SET_FEED = 'SET_FEED';
export const ADD_POST = 'ADD_POST';