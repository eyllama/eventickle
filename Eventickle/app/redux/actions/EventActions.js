// @flow

import * as Types from './Types';
import Constants from '../../utils/Constants';
import Event from '../../models/Event';
import GlobalController from '../../utils/GlobalController';

export const fetchAllEventsForUser = (userID: number) => {
  const url = `${Constants.localUrl}/user/event`;

  return function(dispatch: (any) => void) {
    return fetch(url, {
      method: 'POST',
      headers: Constants.simpleHeaders,
      body: JSON.stringify({ userID }),
    })
      .then((response: Object) => {
        if (!response.ok) {
          throw new Error();
        } else {
          return response.json();
        }
      })
      .then((events: Array<Object>) => {
        const result = [];

        events.forEach((e: Object) => {
          result.push(new Event(e));
        });

        dispatch(emitEvents(result));
      })
      .catch((error: Object) => {
        console.log('Error in (fetchAllEventsForUser, EventActions.js)', error);
        GlobalController.showErrorModal();
      });
  }
};

const emitEvents = (events: Array<Event>) => {
  return {
    type: Types.SET_EVENTS,
    events,
  }
};