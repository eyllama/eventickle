// @flow

import * as types from '../actions/Types';

const initialState = [];

const eventsReducer = (state: Array<Object> = initialState, action: Object): Array<Object> => {
  switch (action.type) {
    case types.SET_EVENTS:
      return [...action.events];
      
    default:
      return state;
  }
};

export default eventsReducer;