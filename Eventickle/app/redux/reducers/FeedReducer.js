import * as Types from '../actions/Types';
import FeedPost from '../../models/FeedPost';

const feedReducer = (state: Array<FeedPost> = [], action: Object) => {
  switch (action.type) {
    case Types.SET_FEED:
      return [...action.feed];
    
    case Types.ADD_POST:
      return [
        action.post,
        ...state,
      ];
      
    default:
      return state;
  }
};

export default feedReducer;