// @flow

import { combineReducers } from 'redux';
import events from './EventReducer';
import feed from './FeedReducer';

const root = combineReducers({
  events,
  feed,
});

export default root;