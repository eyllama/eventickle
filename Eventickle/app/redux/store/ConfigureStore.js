// @flow

import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/RootReducer';

const defaultStore = {};

const configureStore = (initialStore: Object = defaultStore) => {
  const store = createStore(rootReducer, initialStore, compose(
    applyMiddleware(thunk)
  ));
  
  return store;
};

export default configureStore;