// @flow

import { StyleSheet } from 'react-native';
import Device from './utils/Device';

export const COLORS = {
  primary: 'rgb(79, 198, 242)',
  primaryDark: 'rgb(71, 186, 229)',
  primaryLight: 'rgb(89, 218, 247)',
  secondary: 'rgb(239, 122, 231)',
  white: 'rgb(255, 255, 255)',
  black: 'rgb(0, 0, 0)',
  asteroid: 'rgb(69, 87, 91)',
  darkMatter: 'rgb(47, 56, 61)',
  stone: 'rgb(163, 171, 183)',
  marble: 'rgb(120, 128, 140)',
  smoke: 'rgb(239, 239, 239)',
  cotton: 'rgb(242, 246, 249)',
  transparent: 'transparent',
  green: 'rgb(79, 221, 129)',
  orange: 'rgb(239, 182, 47)',
  red: 'rgb(226, 65, 81)',
};

export function convertToOpacity(color: string, opacity: number): string {
  const rgb = color.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
  let r = '';
  let b = '';
  let g = '';

  if (rgb && rgb.length == 4) {
    r = rgb[1];
    g = rgb[2];
    b = rgb[3];
  } else {
    return color;
  }

  return `rgba(${r}, ${g}, ${b}, ${opacity})`;
};

export const STYLES = StyleSheet.create({
  flexOne: {
    flex: 1,
  },

  tabHairline: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: COLORS.smoke,
  },

  containerWithPadding: {
    paddingTop: Device.statusBarHeight,
  },

  whiteContainer: {
    flex: 1,
    backgroundColor: COLORS.white,
  },

  container: {
    flex: 1,
    backgroundColor: COLORS.cotton,
  },

  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.asteroid,
    fontSize: 14,
  },

  grayText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.marble,
    fontSize: 14,
  },

  lightText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.stone,
    fontSize: 14,
  },

  smText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.asteroid,
    fontSize: 12,
  },

  xsText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.asteroid,
    fontSize: 10,
  },

  smGrayText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.marble,
    fontSize: 12,
  },

  smLightText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.stone,
    fontSize: 12,
  },

  xsLightText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.stone,
    fontSize: 10,
  },

  whiteText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.white,
    fontSize: 14,
  },

  smWhiteText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.white,
    fontSize: 12,
  },

  xsWhiteText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.white,
    fontSize: 10,
  },

  lgText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.asteroid,
    fontSize: 16,
  },

  lgLightText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.stone,
    fontSize: 16,
  },

  lgWhiteText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.white,
    fontSize: 16,
  },

  xlText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.asteroid,
    fontSize: 20,
  },

  xlWhiteText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.white,
    fontSize: 20,
  },

  xlLightText: {
    fontFamily: 'AvenirNext-Medium',
    color: COLORS.stone,
    fontSize: 20,
  },

  boldText: {
    fontFamily: 'AvenirNext-DemiBold',
  },

  darkText: {
    color: COLORS.darkMatter,
  },
});