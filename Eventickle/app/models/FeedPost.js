// @flow

class FeedPostRecord {
  pid: number;
  eid: number;
  uid: number;
  type: string;
  comment: string;
  reaction_icon: string;
  reaction_label: string;
  album_new_count: number;
  username: string;
  first_name: string;
  last_name: string;
  profile_picture: string;
  date_created: Date;
}

class FeedPost extends FeedPostRecord {

  constructor(source: Object) {
    super();
    Object.assign(this, source);
  }

  get fullName(): string {
    return this.first_name + ' ' + this.last_name;
  }

  get userInitials(): string {
    const firstInitial = this.first_name.charAt(0);
    const secondInitial = this.last_name.charAt(0);
    return firstInitial + secondInitial;
  }

  isRegularType(): boolean {
    return this.type == 'regular';
  }

  isReactionType(): boolean {
    return this.type == 'reaction';
  }

  isAlbumUpdateType(): boolean {
    return this.type == 'album_update';
  }

}

export default FeedPost;