// @flow

import { COLORS } from '../Styles';

class EventRecord {
  eid: number;
  event_name: string;
  event_description: string;
  cover_photo: string;
  attendance_status: string;
  host_id: number;
  host_avatar: string;
  host_first_name: string;
  start_time: Date;
  date_occurring: Date;
  aid: number;
  street: string;
  zipcode: string;
  name: string;
  coords: { x: number, y: number };
}

class Event extends EventRecord {
  constructor(source: Object) {
    super();
    Object.assign(this, source);
  }

  get coverPhoto(): string {
    if (this.cover_photo) {
      return this.cover_photo;
    } else {
      return '';
    }
  }

  get attendanceStatus(): string {
    if (this.attendance_status) {
      return this.attendance_status;
    } else {
      return 'RSVP';
    }
  }

  get rsvpColor(): string {
    switch (this.attendanceStatus) {
      case 'Going':
        return COLORS.green;
      case 'Maybe':
        return COLORS.orange;
      case 'Cant':
        return COLORS.red;
      default:
        return COLORS.smoke;
    }
  }

}

export default Event;