// @flow

import { StyleSheet } from 'react-native';
import { STYLES, COLORS } from '../../../../Styles';
import Device from '../../../../utils/Device';

export default StyleSheet.create({
  container: {
    margin: 2,
  },

  image: {
    width: (Device.screenWidth - 16) / 3,
    height: (Device.screenWidth - 16) / 3,
  },
});