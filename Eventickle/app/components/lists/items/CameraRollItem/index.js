// @flow

import React from 'react';
import type { Node, Element } from 'react';

import {
  TouchableOpacity,
  View,
  Image,
} from 'react-native';
import { STYLES, COLORS } from '../../../../Styles';
import styles from './styles';
import ImageView from '../../../utils/ImageView';
import Optional from '../../../utils/Optional';

type Props = {
  item: Object,
  updateSelectedUri: (uri: string) => void,
};

class CameraRollItem extends React.PureComponent<Props> {

  _emitPhotoUri = () => {
    this.props.updateSelectedUri(this.props.item.node.image.uri);
  }

  render(): Node {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this._emitPhotoUri}>
        <ImageView
          style={styles.image}
          source={this.props.item.node.image.uri}>
          <View />
        </ImageView>
      </TouchableOpacity>
    );
  }
}

export default CameraRollItem;