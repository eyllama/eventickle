// @flow

import { StyleSheet } from 'react-native';
import { STYLES, COLORS, convertToOpacity } from '../../../../Styles';

export default StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    paddingHorizontal: 24,
    paddingTop: 24,
    paddingBottom: 12,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.smoke,
  },

  authorInfoContainer: {
    marginBottom: 24,
    ...STYLES.row,
  },

  authorAvatar: {
    width: 32,
    height: 32,
    borderRadius: 16,
    marginRight: 18,
    ...STYLES.center,
  },

  optionsContainer: {
    justifyContent: 'flex-end',
    paddingTop: 12,
    ...STYLES.row,
  },

  optionBtn: {
    width: 32,
    height: 32,
    borderRadius: 16,
  },

  // reaction

  reactionContainer: {
    height: 40,
    paddingHorizontal: 12,
    backgroundColor: COLORS.white,
    marginHorizontal: 24,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: COLORS.smoke,
    ...STYLES.row,
  },

  reactionUserName: {
    marginRight: 6,
    ...STYLES.text,
  },

});