// @flow

import React from 'react';
import type { Node, Element } from 'react';

import {
  View,
  TouchableHighlight,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import { STYLES, COLORS } from '../../../../Styles';
import styles from './styles';
import { Navigation } from 'react-native-navigation';
import FeedPost from '../../../../models/FeedPost';

type Props = {
  post: FeedPost,
  goToPost: (FeedPost) => void,
  disabled: boolean,
};

class FeedPostItem extends React.Component<Props> {

  static defaultProps = {
    goToPost: () => {},
    disabled: false,
  };

  _goToPost = () => {
    this.props.goToPost(this.props.post);
  }

  _renderAuthorInfo(): Element<*> {
    return (
      <View style={styles.authorInfoContainer}>
        <Image
          style={styles.authorAvatar}
          source={{ uri: 'https://scontent-sjc3-1.xx.fbcdn.net/v/t1.0-9/13119050_10207598890104205_6189787757873458772_n.jpg?_nc_cat=109&_nc_ht=scontent-sjc3-1.xx&oh=c8105cada3790194a36b289534878791&oe=5CD33495' }}
          resizeMode={'cover'} />
        <Text style={[STYLES.text, STYLES.boldText]}>
          {this.props.post.fullName}
        </Text>
        <View style={STYLES.flexOne} />
        <Text style={[STYLES.smLightText]}>3h</Text>
      </View>
    );
  }

  _renderComment(): Element<*> {
    return (
      <Text style={STYLES.text}>{this.props.post.comment}</Text>
    );
  }

  _renderOptions(): Element<*> {
    return (
      <View style={styles.optionsContainer}>
        <TouchableOpacity
          style={[styles.optionBtn, { marginRight: 24 }]}
          onPress={() => {}}
          activeOpacity={0.8}>
          <Image
            style={styles.optionBtn}
            source={require('../../../../assets/images/dots_icon_stone.png')}
            resizeMode={'cover'} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.optionBtn}
          onPress={() => {}}
          activeOpacity={0.8}>
          <Image
            style={styles.optionBtn}
            source={require('../../../../assets/images/dots_icon_stone.png')}
            resizeMode={'cover'} />
        </TouchableOpacity>
      </View>
    );
  }

  _renderRegularPost(): Element<*> {
    return (
      <TouchableHighlight
        style={{ marginBottom: 6 }}
        onPress={this._goToPost}
        underlayColor={COLORS.asteroid}
        disabled={this.props.disabled}>
        <View style={styles.container}>
          {this._renderAuthorInfo()}
          {this._renderComment()}
          {this._renderOptions()}
        </View>
      </TouchableHighlight>
    );
  }

  _renderReactionPost(): Element<*> {
    return (
      <TouchableHighlight
        style={{ marginBottom: 6 }}
        onPress={() => {}}
        underlayColor={COLORS.asteroid}
        disabled={this.props.disabled}>
        {this._renderReactionContent()}
      </TouchableHighlight>
    );
  }

  _renderReactionContent(): Element<*> {
    return (
      <View style={styles.reactionContainer}>
        <View style={styles.authorAvatar}>
          <Text style={STYLES.text}>{this.props.post.reaction_icon}</Text>
        </View>
        <Text style={styles.reactionUserName}>{`${this.props.post.fullName} is`}</Text>
        <Text style={[STYLES.text, STYLES.boldText, STYLES.flexOne]}>
          {this.props.post.reaction_label}
        </Text>
      </View>
    );
  }

  render(): Node {
    if (this.props.post.isRegularType()) {
      return this._renderRegularPost();
    } else if (this.props.post.isReactionType()) {
      return this._renderReactionPost();
    }
  }
}

export default FeedPostItem;