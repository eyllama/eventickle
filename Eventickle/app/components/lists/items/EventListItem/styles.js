// @flow

import { StyleSheet } from 'react-native';
import { STYLES, COLORS, convertToOpacity } from '../../../../Styles';

export default StyleSheet.create({
  container: {
    paddingVertical: 24,
    backgroundColor: COLORS.white,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: COLORS.smoke,
  },

  coverPhoto: {
    height: 120,
    marginHorizontal: 24,
    borderRadius: 6,
    overflow: 'hidden',
  },

  dateContainer: {
    width: 70,
    height: 28,
    borderRadius: 4,
    backgroundColor: 'rgba(0,0,0,0.4)',
    ...STYLES.center,
    alignSelf: 'flex-end',
    margin: 14,
  },

  detailsContainer: {
    marginTop: 24,
    paddingHorizontal: 24,
  },

  detailsAndHost: {
    marginTop: 24,
    paddingTop: 24,
    marginHorizontal: 24,
    ...STYLES.row,
    borderTopColor: COLORS.smoke,
    borderTopWidth: StyleSheet.hairlineWidth,
  },

  hostInfo: {
    marginTop: 14,
    ...STYLES.row,
    paddingHorizontal: 24,
  },

  avatar: {
    width: 34,
    height: 34,
    borderRadius: 17,
  },

  attendanceBtn: {
    width: 80,
    height: 24,
    borderRadius: 12,
    borderWidth: 1,
    ...STYLES.center,
  },

  attendanceBtnRSVP: {
    borderColor: COLORS.marble,
    backgroundColor: COLORS.white,
  },

  attendanceBtnGoing: {
    borderColor: COLORS.green,
    backgroundColor: convertToOpacity(COLORS.green, 0.4),
  },

  attendanceBtnMaybe: {
    borderColor: COLORS.orange,
    backgroundColor: convertToOpacity(COLORS.orange, 0.4),
  },

  attendanceBtnCant: {
    borderColor: COLORS.red,
    backgroundColor: convertToOpacity(COLORS.red, 0.4),
  },

  attendanceText: {
    fontFamily: 'AvenirNext-Medium',
    fontSize: 10,
    color: COLORS.marble,
    paddingTop: 2,
  },
});