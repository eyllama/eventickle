// @flow

import React from 'react';
import type { Element, Node } from 'react';

import {
  View,
  TouchableHighlight,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import styles from './styles';
import { STYLES, COLORS } from '../../../../Styles';
import GlobalController from '../../../../utils/GlobalController';
import Event from '../../../../models/Event';
import { Navigation } from 'react-native-navigation';
import _ from 'lodash';
import AttendanceModal from '../../../modals/AttendanceModal';
import ErrorModal from '../../../modals/ErrorModal';

type Props = {
  item: Event,
  goToEvent: (Event) => void,
};

class EventListItem extends React.PureComponent<Props> {

  _goToEventDetails = () => {
    this.props.goToEvent(this.props.item);
  }

  _showAttendanceModal = () => {
    const {
      event_name,
      host_id,
      host_first_name,
      host_avatar,
      attendance_status
    } = this.props.item;

    const childProps = { event_name, host_id, host_first_name, host_avatar: 'https://scontent-sjc3-1.xx.fbcdn.net/v/t1.0-9/13442371_10206882141507216_3960960229936601581_n.jpg?_nc_cat=105&oh=dea23472734f866c4ae9f207fd080636&oe=5C504DF2', attendance_status };

    const component = (
      <AttendanceModal {...childProps} />
    );

    GlobalController.showModal('slide', component);
  }

  _goToHostProfile = () => {

  }

  _getAttendanceButtonInfo(): Object {
    const attendance = this.props.item.attendanceStatus;
    const button = {
      styles: {},
      text: { color: COLORS.asteroid },
      underlay: COLORS.stone,
    };

    switch (attendance) {
      case 'Going':
        button.styles = styles.attendanceBtnGoing;
        button.underlay = COLORS.green;
        break;
      case 'Maybe':
        button.styles = styles.attendanceBtnMaybe;
        button.underlay = COLORS.orange;
        break;
      case 'Cant':
        button.styles = styles.attendanceBtnCant;
        button.underlay = COLORS.red;
        break;
      default:
        button.styles = styles.attendanceBtnRSVP;
        button.text.color = COLORS.marble;
    }

    return button;
  }

  _renderTitleAndAttendance(): Element<*> {
    const button = this._getAttendanceButtonInfo();
    
    return (
      <View style={styles.detailsContainer}>
        <View style={STYLES.row}>
          <Text style={[STYLES.lgText, STYLES.flexOne]}>
            {this.props.item.event_name}
          </Text>
          <TouchableHighlight
            style={[styles.attendanceBtn, button.styles]}
            underlayColor={button.underlay}
            onPress={this._showAttendanceModal}>
            <Text style={[styles.attendanceText, button.text]}>
              {this.props.item.attendanceStatus.toUpperCase()}
            </Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  _renderDetailsAndHost(): Element<*> {
    return (
      <View style={styles.detailsAndHost}>
        <View style={STYLES.flexOne}>
          <Text style={STYLES.smLightText}>Friday・NOV 20</Text>
          <Text style={STYLES.smLightText}>9:00 PM</Text>
          <Text style={STYLES.smLightText}>
            {`${_.get(this.props.item, 'street')}, ${_.get(this.props.item, 'zipcode')}`}
          </Text>
        </View>
        {this._renderHostAvatar()}
      </View>
    );
  }

  _renderHostAvatar(): Element<*> {
    return (
      <TouchableOpacity
        onPress={this._goToHostProfile}>
        <Image
          style={styles.avatar}
          // source={{ uri: _.get(this.props.eventItem, 'host_avatar', '') }}
          source={{ uri: 'https://scontent-sjc3-1.xx.fbcdn.net/v/t1.0-9/13442371_10206882141507216_3960960229936601581_n.jpg?_nc_cat=105&oh=dea23472734f866c4ae9f207fd080636&oe=5C504DF2' }}
          resizeMode={'cover'} />
      </TouchableOpacity>
    );
  }

  render(): Node {
    return (
      <TouchableHighlight
        onPress={this._goToEventDetails}
        underlayColor={COLORS.asteroid}>
        <View style={styles.container}>
          <Image
            style={styles.coverPhoto}
            source={{ uri: this.props.item.coverPhoto }}
            resizeMode={'cover'} />
          {this._renderTitleAndAttendance()}
          {this._renderDetailsAndHost()}
        </View>
      </TouchableHighlight>
    );
  }

}

export default EventListItem;