// @flow

import { StyleSheet } from 'react-native';
import { COLORS, STYLES } from '../../../Styles';
import Device from '../../../utils/Device';

export default StyleSheet.create({
  container: {
    ...STYLES.row,
    paddingTop: 12,
    paddingBottom: 12,
    paddingHorizontal: 24,
    zIndex: 10,
  },
  
  statusBar: {
    height: Device.statusBarHeight,
  },

  bottomBorder: {
    borderBottomWidth: 1,
    borderBottomColor: COLORS.smoke,
  },

  shadowIOS: {
    shadowRadius: 4,
    shadowOpacity: 0.2,
    shadowOffset: { width: 0, height: 8 },
    shadowColor: 'rgba(0,0,0,0.4)',
  },

  shadowAndroid: {
    elevation: 2,
  },

});