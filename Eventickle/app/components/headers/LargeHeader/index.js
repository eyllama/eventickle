// @flow

import React from 'react';
import type { Element, Node } from 'react';

import {
  View,
  Text,
  Platform,
} from 'react-native';
import styles from './styles';
import { STYLES, COLORS } from '../../../Styles';

type Props = {
  title: string,
  titleStyle?: ?Object | Array<Object>,
  backgroundColor: string,
  hasShadow: boolean,
  hasBottomBorder: boolean,
  statusBarColor: string,
  rightComponent?: Element<*>,
  children?: ?Node,
};

class LargeHeader extends React.PureComponent<Props> {

  static defaultProps = {
    backgroundColor: COLORS.white,
    hasShadow: false,
    hasBottomBorder: false,
    statusBarColor: COLORS.white,
  };

  _renderRightComponent(): ?Element<*> {
    if (this.props.rightComponent) {
      return this.props.rightComponent;
    } else {
      return null;
    }
  }

  render(): Node {
    return (
      <View>
        <View style={[styles.statusBar, { backgroundColor: this.props.statusBarColor }]} />
        <View style={[
          styles.container,
          this.props.hasBottomBorder ? styles.bottomBorder : null,
          this.props.hasShadow ? Platform.select({
            ios: styles.shadowIOS, android: styles.shadowAndroid
          }) : null,
          { backgroundColor: this.props.backgroundColor }
        ]}>
          <Text style={[STYLES.xlText, STYLES.flexOne, this.props.titleStyle]}>
            {this.props.title}
          </Text>
          {this._renderRightComponent()}
        </View>
      </View>
    );
  }
}

export default LargeHeader;