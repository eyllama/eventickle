// @flow

import React from 'react';
import type { Node, Element } from 'react';

import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { STYLES, COLORS } from '../../../Styles';
import Device from '../../../utils/Device';
import Optional from '../../utils/Optional';
import { Navigation } from 'react-native-navigation';

type Props = {
  title: string,
  hasBackButton: boolean,
  backButtonTitle?: string,
  backButtonComponent?: Element<*>,
  style?: Object,
  statusBarColor: string,
  hasStatusBar: boolean,
  titleTextStyle: Object,
  onBack: () => void,
};

class SimpleHeader extends React.PureComponent<Props> {

  static defaultProps = {
    title: '',
    titleTextStyle: STYLES.text,
    statusBarColor: COLORS.white,
    hasStatusBar: true,
    hasBackButton: true,
  };

  _back = () => {
    this.props.onBack();
  }

  _renderBackButton(): Element<*> {
    if (Boolean(this.props.backButtonTitle)) {
      return (
        <Text style={STYLES.text}>{this.props.backButtonTitle}</Text>
      );
    }
    
    return (
      <Image
        style={styles.headerBtnIcon}
        source={require('../../../assets/images/dots_icon_stone.png')}
        resizeMode={'cover'} />
    );
  }

  _renderTitleContainer(): Element<*> {
    return (
      <View style={styles.titleContainer}>
        <Text style={this.props.titleTextStyle}>
          {this.props.title}
        </Text>
      </View>
    );
  }

  render(): Node {
    return (
      <View>
        <Optional condition={this.props.hasStatusBar}>
          <View style={[styles.statusBar, { backgroundColor: this.props.statusBarColor}]} />
        </Optional>
        <View style={[styles.container, this.props.style]}>
          <TouchableOpacity
            style={styles.headerBtn}
            onPress={this._back}
            activeOpacity={0.8}>
            <Optional condition={this.props.hasBackButton}>
              {this._renderBackButton()}
            </Optional>
          </TouchableOpacity>
          {this._renderTitleContainer()}
          <TouchableOpacity
            style={styles.headerBtn}
            onPress={() => {}}
            activeOpacity={0.8}>

          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 54,
    backgroundColor: COLORS.white,
    paddingHorizontal: 12,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.smoke,
    ...STYLES.row,
  },

  statusBar: {
    height: Device.statusBarHeight,
  },

  headerBtn: {
    width: 50,
    height: 54,
    // backgroundColor: COLORS.primary,
    ...STYLES.center,
  },

  headerBtnIcon: {
    width: 32,
    height: 32,
  },

  titleContainer: {
    flex: 1,
    height: 54,
    ...STYLES.center,
  },
});

export default SimpleHeader;
