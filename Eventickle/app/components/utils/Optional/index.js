// @flow

import React from 'react';
import type { Element, Node } from 'react';

import { View } from 'react-native';

type Props = {
  condition: boolean,
  children: Node,
};

function Optional(props: Props): ?Element<*> {
  if (props.condition) {
    return (
      <View>
        {props.children}
      </View>
    );
  } else {
    return null;
  }
};

export default Optional;