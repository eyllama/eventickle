// @flow

import { StyleSheet } from 'react-native';
import { STYLES, COLORS } from '../../../Styles';

export default StyleSheet.create({
  container: {
    height: 50,
    backgroundColor: COLORS.white,
    ...STYLES.tabHairline,
  },

  contentContainer: {
    ...STYLES.row,
  },

  pageItemContainer: {
    flex: 1,
    height: 48,
    ...STYLES.center,
  },

  selector: {
    height: 2,
    borderRadius: 1,
  },
});