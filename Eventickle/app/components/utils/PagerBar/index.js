// @flow

import React from 'react';
import type { Node, Element } from 'react';

import {
  View,
  Animated,
  Easing,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import { STYLES, COLORS } from '../../../Styles';
import styles from './styles';
import Device from '../../../utils/Device';

const SCREEN_WIDTH = Device.screenWidth;

type Props = {
  pages: Array<string>,
  selectorColor: string,
  selectorWidth: number,
  animatedValue: Animated.Value,
  onTabPress: (number) => void,
};

type State = {
  selected: number,
};

class PagerBar extends React.PureComponent<Props, State> {

  static defaultProps = {
    pages: ['First', 'Second'],
    selectorColor: COLORS.asteroid,
    selectorWidth: Device.screenWidth / 2,
  };

  state = {
    selected: 0,
  };

  _onTabPress = (index: number) => {
    this.setState({ selected: index });
    this.props.onTabPress(index);
  }

  _renderItem = (page: string, index: number) => {
    return (
      <TouchableOpacity
        key={index}
        style={styles.pageItemContainer}
        onPress={this._onTabPress.bind(this, index)}
        activeOpacity={0.8}>
        <Text style={STYLES.smText}>{page}</Text>
      </TouchableOpacity>
    );
  }

  _renderContent(): Array<Element<*>> {
    return this.props.pages.map((page: string, index: number): Element<*> => {
      return this._renderItem(page, index);      
    });
  }

  render(): Element<*> {
    const translateX = this.props.animatedValue.interpolate({
      inputRange: [0, SCREEN_WIDTH],
      outputRange: [0, SCREEN_WIDTH / 2],
      extrapolate: 'clamp',
    });

    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          {this._renderContent()}
        </View>
        <Animated.View style={[
          styles.selector,
          {
            width: this.props.selectorWidth,
            backgroundColor: this.props.selectorColor,
            transform: [
              { translateX },
              { scaleX: 0.75 }
            ]
          },
        ]} />
      </View>
    );
  }

}

export default PagerBar;