// @flow

import React from 'react';
import type { Node, Element } from 'react';

import { View, StyleSheet } from 'react-native';
import { STYLES, COLORS } from '../../../Styles';
import Device from '../../../utils/Device';

type Props = {
  style: Object,
  backgroundColor: string,
  statusBarColor: string,
  children?: ?Node,
};

function StatusBarView(props: Props): Element<*> {
  return (
    <View style={[
      props.style, STYLES.flexOne,
      { backgroundColor: props.backgroundColor }
    ]}>
      <View style={[styles.statusBar, { backgroundColor: props.statusBarColor }]} />
      {props.children}
    </View>
  );
};

StatusBarView.defaultProps = {
  backgroundColor: COLORS.white,
  statusBarColor: COLORS.primary,
  style: {},
};

const styles = StyleSheet.create({
  statusBar: {
    height: Device.statusBarHeight,
  },
});

export default StatusBarView;
