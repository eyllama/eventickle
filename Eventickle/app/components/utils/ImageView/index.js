// @flow

import React from 'react';
import type { Node, Element } from 'react';

import { View, Image, StyleSheet } from 'react-native';
import { STYLES } from '../../../Styles';

type Props = {
  style: Object,
  source: string,
  resizeMode: 'cover' | 'contain' | 'stretch',
  children?: ?Node,
};

function ImageView(props: Props): Element<*> {
  return (
    <View style={props.style}>
      <Image
        style={styles.image}
        source={{ uri: props.source }}
        resizeMode={props.resizeMode} />
      {props.children}
    </View>
  );
};

ImageView.defaultProps = {
  resizeMode: 'cover',
};

const styles = StyleSheet.create({
  image: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});

export default ImageView;