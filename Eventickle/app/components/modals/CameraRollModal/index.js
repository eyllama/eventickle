// @flow

import React from 'react';
import type { Node, Element } from 'react';

import {
  View,
  CameraRoll,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import { STYLES, COLORS } from '../../../Styles';
import SimpleHeader from '../../headers/SimpleHeader';
import { Navigation } from 'react-native-navigation';
import GlobalController from '../../../utils/GlobalController';
import CameraRollItem from '../../lists/items/CameraRollItem';

type Props = {
  onDismiss: () => void,
  onSelectUri: (uri: string) => void,
};

type State = {
  photos: Array<Object>,
};

class CameraRollModal extends React.PureComponent<Props, State> {

  static defaultProps = {
    onDismiss: () => {},
    onSelectUri: () => {},
  };

  state = {
    photos: [],
  };

  async componentDidMount() {
    try {
      const photos = await CameraRoll.getPhotos({
        first: 400,
        assetType: 'Photos',
      });

      if (photos) {
        this.setState({ photos: photos.edges });
      } else {
        throw new Error();
      }
    } catch (e) {
      GlobalController.showErrorModal({
        message: "Hmm...we couldn't get a hold of your photos. Please try again later",
      });
    }
  }

  _cancel = () => {
    this.props.onDismiss();
  }

  _keyExtractor = (item: Object, index: number): string => index.toString();

  _updateUri = (uri: string) => {
    this.props.onSelectUri(uri);
  }

  _renderContent(): Element<*> {
    if (!this.state.photos.length) {
      return this._renderLoading();
    } else {
      return this._renderPhotos();
    }
  }

  _renderLoading(): Element<*> {
    return (
      <View style={[STYLES.whiteContainer, STYLES.center]}>
        <ActivityIndicator size={'large'} color={COLORS.asteroid} />
      </View>
    );
  }

  _renderPhotos(): Element<*> {
    return (
      <FlatList
        data={this.state.photos}
        keyExtractor={this._keyExtractor}
        renderItem={this._renderPhotoItem}
        numColumns={3}
        contentContainerStyle={{ padding: 2 }}
        initialNumToRender={12} />
    );
  }

  _renderPhotoItem = ({ item }: Object): Element<*> => {
    return (
      <CameraRollItem item={item} updateSelectedUri={this._updateUri} />
    );
  }

  render(): Node {
    return (
      <View style={STYLES.whiteContainer}>
        <SimpleHeader
          title={'Your Photos'}
          onBack={this._cancel}
          hasStatusBar={false}/>
        {this._renderContent()}
      </View>
    );
  }
}

export default CameraRollModal;