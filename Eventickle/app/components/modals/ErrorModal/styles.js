// @flow

import { StyleSheet } from 'react-native';
import { STYLES, COLORS } from '../../../Styles';

export default StyleSheet.create({
  container: {
    width: 300,
    padding: 24,
    ...STYLES.center,
  },

  titleText: {
    marginBottom: 24,
  },

  messageText: {
    textAlign: 'center',
    marginBottom: 32,
  },

  button: {
    width: 120,
    height: 34,
    borderRadius: 17,
    backgroundColor: COLORS.primary,
    ...STYLES.center,
  },

});