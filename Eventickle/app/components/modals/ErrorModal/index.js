// @flow

import React from 'react';
import type { Node, Element } from 'react';

import {
  View,
  Text,
  TouchableHighlight,
  Image,
} from 'react-native';
import { STYLES, COLORS } from '../../../Styles';
import styles from './styles';
import Optional from '../../utils/Optional';

type Props = {
  title: string,
  titleStyle: Object,
  message: string,
  messageStyle: Object,
  bodyContents?: ?Element<*>,
  buttonTitle: string,
  buttonTitleStyle: Object,
  buttonStyle: Object,
  onOkPress: () => void,
};

function ErrorModal(props: Props): Element<*> {
  return (
    <View style={styles.container}>
      <Text style={[STYLES.xlText, STYLES.darkText, styles.titleText, props.titleStyle]}>{props.title}</Text>
      <Optional condition={!!props.bodyContents}>
        {props.bodyContents}
      </Optional>
      <Optional condition={!props.bodyContents}>
        <Text style={[STYLES.text, styles.messageText]}>{props.message}</Text>
      </Optional>
      <TouchableHighlight
        style={[styles.button, props.buttonStyle]}
        underlayColor={COLORS.primaryDark}
        onPress={props.onOkPress.bind(this)}>
        <Text style={[STYLES.smWhiteText, props.buttonTitleStyle]}>
          {props.buttonTitle}
        </Text>
      </TouchableHighlight>
    </View>
  );
};

ErrorModal.defaultProps = {
  title: '⚠️',
  titleStyle: {},
  message: 'Looks like something went wrong 😬\n\nPlease try again later',
  messageStyle: {},
  buttonTitle: 'OK',
  buttonTitleStyle: {},
  buttonStyle: {},
  onOkPress: () => {},
};

export default ErrorModal;
