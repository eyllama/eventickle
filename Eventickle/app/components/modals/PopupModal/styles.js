// @flow

import { StyleSheet } from 'react-native';
import { STYLES, COLORS, convertToOpacity } from '../../../Styles';

export default StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(1, 9, 30, 0.6)',
    ...STYLES.center,
  },

  container: {
    backgroundColor: COLORS.white,
    borderRadius: 4,
  },

  shadowIOS: {
    shadowRadius: 12,
    shadowOpacity: 0.2,
    shadowOffset: { width: 0, height: 10 },
    shadowColor: 'rgba(1, 9, 30, 0.6)',
  },

  shadowAndroid: {
    elevation: 2,
  },
});