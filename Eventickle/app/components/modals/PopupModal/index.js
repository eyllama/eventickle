// @flow

import React from 'react';
import type { Node, Element } from 'react';

import {
  View,
  Animated,
  Easing,
  TouchableHighlight,
  TouchableOpacity,
  StyleSheet,
  Platform,
} from 'react-native';
import { STYLES, COLORS } from '../../../Styles';
import styles from './styles';
import GlobalController from '../../../utils/GlobalController';
import Constants from '../../../utils/Constants';
import { Navigation } from 'react-native-navigation';

type Props = {
  onShow: () => void,
  onDismiss: () => void,
  child: Element<*>,
  componentId: string,
};

type State = {
  scaleValue: Animated.Value,
  overlayOpacity: Animated.Value,
};

class PopupModal extends React.PureComponent<Props, State> {

  _animation: any = null;

  static defaultProps = {
    onShow: () => {},
    onDismiss: () => {},
  };

  state = {
    scaleValue: new Animated.Value(0),
    overlayOpacity: new Animated.Value(0),
  };

  componentDidMount() {
    this._animateIn();
  }

  _hideModal = () => {
    this.state.overlayOpacity.setValue(1);
    this._animateOverlay(0).start(() => {
      this.props.onDismiss();
      GlobalController.hideModal(this.props.componentId);
    });
  }

  _animateIn = () => {
    if (this._animation) {
      this._animation.stop();
    }

    this.state.scaleValue.setValue(0);
    this.state.overlayOpacity.setValue(0);

    this._animation = Animated.parallel([
      this._animateModal(),
      this._animateOverlay(1),
    ]);

    this._animation.start(() => this.props.onShow());
  }

  _animateModal = () => {
    return Animated.timing(
      this.state.scaleValue,
      {
        toValue: 1,
        duration: 250,
        easing: Constants.simpleElasticEase,
        useNativeDriver: true,
      }
    );
  }

  _animateOverlay = (toValue: number) => {
    return Animated.timing(
      this.state.overlayOpacity,
      {
        toValue: toValue,
        duration: 200,
        easing: Constants.simpleEaseOut,
        useNativeDriver: true,
      }
    );
  }

  render(): Node {
    return (
      <Animated.View
        style={[
          StyleSheet.absoluteFill,          
          { opacity: this.state.overlayOpacity }
        ]}>
        <TouchableOpacity
          style={[StyleSheet.absoluteFill, styles.overlay]}
          onPress={this._hideModal}
          activeOpacity={1}>
          <Animated.View style={[
            styles.container,
            Platform.OS == 'ios' ? styles.shadowIOS : styles.shadowAndroid,
            { transform: [{ scale: this.state.scaleValue }] },
          ]}>
            <TouchableOpacity
              onPress={() => {}}
              activeOpacity={1}>
              {
                React.cloneElement(this.props.child, { onOkPress: this._hideModal })
              }
            </TouchableOpacity>
          </Animated.View>
        </TouchableOpacity>
      </Animated.View>
    );
  }
}

export default PopupModal;