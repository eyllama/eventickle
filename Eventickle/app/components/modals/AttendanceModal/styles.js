/* @flow */

import { StyleSheet } from 'react-native';
import { STYLES, COLORS, convertToOpacity } from '../../../Styles';

export default StyleSheet.create({
  container: {
    flex: 1,
    bottom: 0,
  },

  topSection: {
    ...STYLES.center,
    borderBottomWidth: 1,
    borderColor: COLORS.smoke,
    marginBottom: 24,
    marginHorizontal: 24,
  },

  avatars: {
    paddingVertical: 24,
    ...STYLES.row,
    justifyContent: 'center',
  },

  avatar: {
    width: 36,
    height: 36,
    borderRadius: 18,
    marginHorizontal: 14,
  },

  line: {
    width: 46,
    height: 4,
    borderRadius: 2,
    backgroundColor: COLORS.smoke,
  },

  attendanceContainer: {
    marginTop: 24,
    ...STYLES.row,
    justifyContent: 'space-around',
  },

  eventFlexContainer: {
    flex: 1,
    ...STYLES.center,
  },

  attendanceBtn: {
    ...STYLES.center,
    paddingLeft: 2,
    width: 46,
    height: 46,
    borderRadius: 23,
    borderWidth: 1,
  },

  goingBtn: {
    borderColor: COLORS.green,
    backgroundColor: convertToOpacity(COLORS.green, 0.4),
  },

  maybeBtn: {
    borderColor: COLORS.orange,
    backgroundColor: convertToOpacity(COLORS.orange, 0.4),
  },

  cantBtn: {
    borderColor: COLORS.red,
    backgroundColor: convertToOpacity(COLORS.red, 0.4),
  },

  selectors: {
    marginTop: 24,
    ...STYLES.row,
  },

  selector: {
    width: 14,
    height: 2,
    borderRadius: 1,
    backgroundColor: COLORS.primary,
  },

  closeBtn: {
    paddingHorizontal: 24,
    height: 32,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: COLORS.marble,
    ...STYLES.center,
    alignSelf: 'center',
    marginTop: 34,
  },

});
