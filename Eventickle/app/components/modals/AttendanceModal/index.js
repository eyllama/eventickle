// @flow

import React from 'react';
import type { Element, Node } from 'react';

import {
  View,
  Image,
  Text,
  TouchableHighlight,
} from 'react-native';
import styles from './styles';
import { STYLES, COLORS } from '../../../Styles';
import GlobalController from '../../../utils/GlobalController';
import _ from 'lodash';

const STATUS_BUTTONS_INFO = [
  { status: 'Going', emoji: '😄', style: 'goingBtn', underlayColor: COLORS.green },
  { status: 'Maybe', emoji: '😶', style: 'maybeBtn', underlayColor: COLORS.orange },
  { status: 'Cant', emoji: '🙁', style: 'cantBtn', underlayColor: COLORS.red },
];

const SELECTORS = [
  { status: 'Going' }, { status: 'Maybe' }, { status: 'Cant' },
];

type Props = {
  style: Object,
  host_id: number,
  host_first_name: string,
  event_name: string,
  attendance_status: string,
  onDismiss: () => void,
};

type State = {
  status: string,
};

class AttendanceModal extends React.PureComponent<Props, State> {

  _closeOrSave: string = 'Close';

  static defaultProps ={ 
    style: {},
    onShow: () => {},
    onDismiss: () => {},
  };

  state = {
    status: this.props.attendance_status,
  };

  _onDismiss = () => {
    this.props.onDismiss();
  }

  _updateAttendance = (status: string) => {
    if (status != this.props.attendance_status) {
      this._closeOrSave = 'Save';
    } else {
      this._closeOrSave = 'Close';
    }
    this.setState({ status });
  }

  _renderTopSection(): Element<*> {
    return (
      <View style={styles.topSection}>
        <View style={styles.avatars}>
          <Image
            style={styles.avatar}
            source={{ uri: 'https://scontent-sjc3-1.xx.fbcdn.net/v/t1.0-9/13119050_10207598890104205_6189787757873458772_n.jpg?_nc_cat=109&oh=d3a36d003b4955e87c423d88b073eb33&oe=5C5C8D95' }}
            resizeMode={'cover'} />
          <View style={styles.line} />
          <Image
            style={styles.avatar}
            source={{ uri: _.get(this.props, 'host_avatar', '') }}
            resizeMode={'cover'} />
        </View>
      </View>
    );
  }

  _renderAttendanceButtons(): Element<any> {
    return (
      <View style={styles.attendanceContainer}>
        {
          STATUS_BUTTONS_INFO.map(
            (button: Object, index: number): Element<*> => {
              return (
                <View key={index}
                  style={[styles.eventFlexContainer, { height: 46 }]}>
                  <TouchableHighlight
                    style={[styles.attendanceBtn, styles[button.style]]}
                    onPress={this._updateAttendance.bind(this, button.status)}
                    underlayColor={button.underlayColor}>
                    <Text>{button.emoji}</Text>
                  </TouchableHighlight>
                </View>

              );
            })
        }
      </View>
    );
  }

  _renderSelectors(): Element<*> {
    return (
      <View style={styles.selectors}>
        {
          SELECTORS.map(
            (selector: Object, index: number): Element<*> => {
              return (
                <View key={index}
                  style={[styles.eventFlexContainer, {
                    height: 2,
                    opacity: this.state.status == selector.status ? 1 : 0,
                  }]}>
                  <View style={styles.selector} />
                </View>
              );
            })
        }
      </View>
    );
  }

  _renderCloseOrSaveBtn(): Element<*> {
    const onPress = this._closeOrSave == 'Save' ? this._onSave : this._onDismiss;

    return (
      <TouchableHighlight
        style={styles.closeBtn}
        underlayColor={COLORS.stone}
        onPress={onPress}>
        <Text style={STYLES.smGrayText}>{this._closeOrSave}</Text>
      </TouchableHighlight>
    );
  }

  render(): Node {
    return (
      <View style={[styles.container, this.props.style]}>
        {this._renderTopSection()}
        <View style={STYLES.center}>
          <Text style={STYLES.lgText}>{this.props.event_name}</Text>
          <Text style={STYLES.smLightText}>
            {`Let ${_.get(this.props, 'host_first_name', 'people')} know!`}
          </Text>
        </View>
        {this._renderAttendanceButtons()}
        {this._renderSelectors()}
        {this._renderCloseOrSaveBtn()}
      </View>
    );
  }

}

export default AttendanceModal;