// @flow

import React from 'react';
import type { Element, Node } from 'react'

import {
  View,
  Animated,
  Easing,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { STYLES, COLORS } from '../../../Styles';
import styles from './styles';
import Constants from '../../../utils/Constants';
import { Navigation } from 'react-native-navigation';
import GlobalController from '../../../utils/GlobalController';

type Props = {
  onShow: () => void,
  onDismiss: () => void,
  component: string,
  height: number,
  childProps: Object,
  componentId: string,
  child: Element<*>,
};

type State = {
  overlayOpacity: Animated.Value,
  contentTranslate: Animated.Value,
};

class SlideModal extends React.PureComponent<Props, State> {
  
  animation: any = null;

  static defaultProps = {
    onShow: () => {},
    onDismiss: () => {},
  }

  state = {
    overlayOpacity: new Animated.Value(0),
    contentTranslate: new Animated.Value(0),
  };

  componentDidMount() {
    this._onShow();
  }

  _onShow = () => {
    if (this.animation) {
      this.animation.stop();
    }

    this.state.overlayOpacity.setValue(0);
    this.state.contentTranslate.setValue(0);

    this.animation = Animated.parallel([
      this._animateOverlay(1),
      this._translateContent(1),
    ]);

    this.animation.start(() => this.props.onShow());
  }

  _onDismiss = () => {
    if (this.animation) {
      this.animation.stop();
    }

    this.state.overlayOpacity.setValue(1);
    this.state.contentTranslate.setValue(1);

    this.animation = Animated.parallel([
      this._translateContent(0),
      this._animateOverlay(0),
    ]);

    this.animation.start(() => {
      this.props.onDismiss();
      GlobalController.hideModal(this.props.componentId);
    });
  }

  _animateOverlay = (toValue: number) => {
    return Animated.timing(
      this.state.overlayOpacity,
      {
        toValue,
        duration: 200,
        easing: Easing.linear,
        delay: toValue == 0 ? 100 : 0,
        useNativeDriver: true,
      },
    );
  }

  _translateContent = (toValue: number) => {
    return Animated.timing(
      this.state.contentTranslate,
      {
        toValue,
        duration: toValue == 0 ? 250 : 350,
        easing: toValue == 0
          ? Constants.simpleEaseIn
          : Constants.simpleEaseOut,
        delay: toValue == 1 ? 100 : 0,
        useNativeDriver: true,
      },
    );
  }

  render(): Node {
    const translateY = this.state.contentTranslate.interpolate({
      inputRange: [0, 1],
      outputRange: [this.props.height, 0],
      extrapolate: 'clamp',
    });

    return (
      <Animated.View
        style={[
          StyleSheet.absoluteFill,
          { opacity: this.state.overlayOpacity },
        ]}>
        <View
          style={[styles.overlay, StyleSheet.absoluteFill]}>
          <Animated.View style={{ transform: [{ translateY }] }}>
            <View
              style={[
                styles.contentContainer,
                { minHeight: this.props.height },
              ]}>
              {
                React.cloneElement(this.props.child, { onDismiss: this._onDismiss })
              }
            </View>
          </Animated.View>
        </View>
      </Animated.View>
    );
  }

}

export default SlideModal;