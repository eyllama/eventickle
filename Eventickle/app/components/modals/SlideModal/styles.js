/*
* @flow
*/

import { StyleSheet } from 'react-native';
import { STYLES, COLORS } from '../../../Styles';
import Device from '../../../utils/Device';

export default StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(1, 9, 30, 0.6)',
    justifyContent: 'flex-end',
  },

  contentContainer: {
    backgroundColor: COLORS.white,
    width: Device.screenWidth,
  },

});
