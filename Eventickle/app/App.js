import { Navigation } from 'react-native-navigation';
import { registerScreens } from './Screens';
import { COLORS } from './Styles';
import configureStore from './redux/store/ConfigureStore';
import { Provider } from 'react-redux';

const store = configureStore();

export function init(): void {
  registerScreens(store, Provider);

  Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setDefaultOptions({
      topBar: {
        visible: false,
        drawBehind: true,
        backButton: {
          visible: false,
        },
      },

      bottomTabs: {
        visible: true,
        animate: true,
        backgroundColor: COLORS.white,
        hideShadow: true,
      },

      bottomTab: {
        textColor: COLORS.stone,
        selectedTextColor: COLORS.primary,
        fontFamily: 'AvenirNext-Medium',
        fontSize: 10,
        iconInsets: { top: 6, left: 0, bottom: -6, right: 0 },
        iconColor: COLORS.stone,
        selectedIconColor: COLORS.primary,
      },

      animations: {
        startApp: {
          y: {
            from: 1000,
            to: 0,
            duration: 500,
            interpolation: 'accelerate',
          },
          alpha: {
            from: 0,
            to: 1,
            duration: 500,
            interpolation: 'accelerate',
          },
        },
      },
      
      push: {
        waitForRender: false,
      },

      popGesture: true,

    });

    Navigation.setRoot({
      root: {
        bottomTabs: {
          children: [
            {
              stack: {
                id: 'EventTabStack',
                children: [
                  { component: { name: 'FeedPostDetails' } },
                  { component: { name: 'EventDetails', } },
                  { component: { name: 'Events' } },
                ],
                options: {
                  bottomTab: {
                    icon: require('./assets/images/dots_icon_stone.png'),
                  },
                },
              },
            }
          ],
        },
      },
    });
  });
}