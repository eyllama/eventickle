// @flow

import { StyleSheet } from 'react-native';
import { STYLES, COLORS } from '../../Styles';

export default StyleSheet.create({
  section: {
    padding: 24,
    marginBottom: 6,
    backgroundColor: COLORS.white,
    ...STYLES.tabHairline,
  },

  sectionHeader: {
    marginBottom: 24,
    ...STYLES.xsLightText,
  },

  coverPhotoPlaceholder: {
    height: 180,
    backgroundColor: COLORS.smoke,
    borderRadius: 6,
    marginBottom: 24,
  },

  uploadOptionBtn: {
    width: 32,
    height: 32,
    borderRadius: 16,
    marginLeft: 24,
    ...STYLES.center,
  },

  uploadOptionIcon: {
    width: 24,
    height: 24,
    tintColor: COLORS.white,
  },

  label: {
    marginBottom: 12,
    ...STYLES.lgLightText,
  },

  input: {
    paddingBottom: 4,
    paddingHorizontal: 12,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.smoke,
    ...STYLES.text,
  },

  largeInput: {
    paddingBottom: 4,
    // paddingHorizontal: 12,
    // borderBottomWidth: 1,
    // borderBottomColor: COLORS.smoke,
    ...STYLES.xlText,
  },

  descriptionInput: {
    // marginTop: 12,
    borderRadius: 4,
    //minHeight: 60,
    ...STYLES.grayText,
  },
});