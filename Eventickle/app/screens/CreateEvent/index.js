// @flow

import React from 'react';
import type { Node, Element } from 'react';

import {
  View,
  ScrollView,
  Text,
  Image,
  TextInput,
  TouchableHighlight,
  Platform,
  Keyboard,
} from 'react-native';
import styles from './styles';
import { STYLES, COLORS } from '../../Styles';
import { Navigation } from 'react-native-navigation';
import SimpleHeader from '../../components/headers/SimpleHeader';
import GlobalController from '../../utils/GlobalController';
import CameraRollModal from '../../components/modals/CameraRollModal';

type Props = {
  componentId: string,
};

type State = {
  coverPhotoUri: string,
};

class CreateEvent extends React.PureComponent<Props, State> {

  _titleInput: *;
  _descriptionInput: *;
  _focusOnDescription: () => void;

  constructor(props: Props) {
    super(props);
    this.state = {
      coverPhotoUri: '',
    };

    this._focusOnDescription = this._focusOn.bind(this, '_descriptionInput');
  }

  _dismiss = () => {
    Navigation.dismissModal(this.props.componentId);
  }

  _goToCamera = () => {

  }

  _goToCameraRoll = () => {
    const modal = (
      <CameraRollModal onSelectUri={this._onSelectCoverPhotoUri} />
    );

    GlobalController.showModal('slide', modal, 400);
  }

  _onSelectCoverPhotoUri = (uri: string) => {
    this.setState({ coverPhotoUri: uri });
  }

  _focusOn = (inputName: string) => {
    if (this[inputName]) {
      this[inputName].focus();
    }
  }

  _renderHeader(): Element<*> {
    return ( 
      <SimpleHeader
        onBack={this._dismiss}
        title={'Create'} />
    );
  }

  _renderCoverPhotoSection(): Element<*> {
    return (
      <View style={styles.section}>
        <Text style={styles.sectionHeader}>{'COVER PHOTO'}</Text>
        {this._renderCoverPhoto()}
        <View style={STYLES.row}>
          <Text style={[STYLES.text, STYLES.flexOne]}>Upload</Text>
          {this._renderCoverPhotoUploadOption(0)}
          {this._renderCoverPhotoUploadOption(1)}
        </View>
      </View>
    );
  }

  _renderCoverPhoto(): Element<*> {
    if (!this.state.coverPhotoUri.length) {
      return (
        <View style={styles.coverPhotoPlaceholder}>

        </View>
      );
    }

    return (
      <Image
        style={styles.coverPhotoPlaceholder}
        source={{ uri: this.state.coverPhotoUri }}
        resizeMode={'cover'} />
    );
  }

  _renderCoverPhotoUploadOption(index: number): Element<*> {
    const backgroundColor = index == 0 ? COLORS.asteroid : COLORS.darkMatter;
    const onPress = index == 0 ? this._goToCamera : this._goToCameraRoll;

    return (
      <TouchableHighlight
        style={[styles.uploadOptionBtn, { backgroundColor }]}
        onPress={onPress}
        underlayColor={COLORS.darkMatter}>
        <Image
          style={styles.uploadOptionIcon}
          source={require('../../assets/images/dots_icon_stone.png')}
          resizeMode={'cover'} />
      </TouchableHighlight>
    );
  }

  _renderTitleSection(): Element<*> {
    return (
      <View style={styles.section}>
        <Text style={styles.sectionHeader}>{'NAME & DESCRIPTION'}</Text>
        <TextInput
          ref={(input: *) => { this._titleInput = input; }}
          style={styles.largeInput}
          placeholder={"What's this event called?"}
          placeholderTextColor={COLORS.stone}
          returnKeyType={'next'}
          onSubmitEditing={this._focusOnDescription} />
        <TextInput
          ref={(input: *) => { this._descriptionInput = input; }}
          style={styles.descriptionInput}
          selectionColor={COLORS.primary}
          placeholder={'What should people know about this event?'}
          placeholderTextColor={COLORS.stone}
          multiline />
      </View>
    );
  }

  render(): Node {
    return (
      <View style={STYLES.container}>
        {this._renderHeader()}
        <ScrollView contentContainerStyle={{ paddingBottom: 60 }}>
          {this._renderCoverPhotoSection()}
          {this._renderTitleSection()}
        </ScrollView>
      </View>
    );
  }

}

export default CreateEvent;
