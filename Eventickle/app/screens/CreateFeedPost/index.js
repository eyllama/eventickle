// @flow

import React from 'react';
import type { Node, Element } from 'react';
import { connect } from 'react-redux';

import {
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  KeyboardAvoidingView,
  TouchableHighlight,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import { STYLES, COLORS, convertToOpacity } from '../../Styles';
import styles from './styles';
import StatusBarView from '../../components/utils/StatusBarView';
import { Navigation } from 'react-native-navigation';
import GlobalController from '../../utils/GlobalController';
import Device from '../../utils/Device';
import Constants from '../../utils/Constants';
import FeedPost from '../../models/FeedPost';
import * as FeedActions from '../../redux/actions/FeedActions';

const POST_TYPES = [
  { type: 'Regular', icon: require('../../assets/images/dots_icon_stone.png') },
  { type: 'Reaction', icon: require('../../assets/images/dots_icon_stone.png') },
  { type: 'Album Update', icon: require('../../assets/images/dots_icon_stone.png') },
];

const EMOJI_REACTIONS = Constants.emojiReactions;

type Props = {
  componentId: string,
  dispatch: (any) => void,
};

type State = {
  postType: string,
  comment: string,
  reaction: Object,
  reactionIcon: string,
  reactionLabel: string,
};

class CreateFeedPost extends React.PureComponent<Props, State> {

  _regularInput: * = null;
  _reactionInput: * = null;
  _pager: * = null;

  state = {
    postType: 'Regular',
    comment: '',
    reaction: EMOJI_REACTIONS[1],
    reactionIcon: EMOJI_REACTIONS[1].icon,
    reactionLabel: '',
  };

  _onPost = () => {
    let comment, label, icon = null;

    switch (this.state.postType) {
      case 'Regular':
        comment = this.state.comment;
        break;
      case 'Reaction':
        icon = this.state.reactionIcon;
        label = this.state.reactionLabel;
        break;
    }

    const post = new FeedPost({
      eid: 1,
      uid: 1,
      type: this.state.postType.toLowerCase(),
      comment,
      reaction_icon: icon,
      reaction_label: label,
      username: 'eyllama',
      first_name: 'Eylam',
      last_name: 'Fried',
      profile_picture: '',
    });

    this.props.dispatch(FeedActions.createPost(post));
  }

  _close = () => {
    Keyboard.dismiss();
    Navigation.dismissModal(this.props.componentId);
  }

  _onPostTypePress = (type: string, index: number) => {
    const comment = type == 'Regular' ? this.state.comment : '';
    const reactionLabel = type == 'Reaction' ? this.state.reactionLabel : '';

    this._pager.scrollTo({ x: Device.screenWidth * index, y: 0, animated: true });
    this.setState({ postType: type, comment, reactionLabel }, () => {
      if (type == 'Regular') {
        this._regularInput.focus();
      } else if (type == 'Reaction') {
        this._reactionInput.focus();
      }
    });
  }

  _onRegularPostChangeText = (comment: string) => {
    this.setState({ comment });
  }

  _canPost(): boolean {
    switch (this.state.postType) {
      case 'Regular':
        return Boolean(this.state.comment);
      case 'Reaction':
        return Boolean(this.state.reactionIcon && this.state.reactionLabel);
      default:
        return false;
    }
  }

  _selectReaction = (emoji: Object) => {
    this.setState({ reactionIcon: emoji.icon, reaction: emoji });
  }

  _onReactionLabelChange = (label: string) => {
    this.setState({ reactionLabel: label });
  }

  _renderHeader(): Element<*> {
    return (
      <View style={styles.header}>
        <TouchableOpacity
          style={[styles.closeBtn, STYLES.center]}
          onPress={this._close}
          activeOpacity={0.7}>
          <Image
            style={styles.closeBtn}
            source={require('../../assets/images/dots_icon_stone.png')}
            resizeMode={'cover'} />
        </TouchableOpacity>
        <Text
          style={styles.titleText}
          numberOfLines={1}>
          {this.state.postType}
        </Text>
        {this._renderPostTypes()}
      </View>
    );
  }

  _renderPostTypes(): Array<Element<*>> {
    return POST_TYPES.map((item: Object, index: number): Element<*> => {
      return (
        <TouchableOpacity
          key={index}
          style={styles.postTypeBtn}
          onPress={this._onPostTypePress.bind(this, item.type, index)}
          activeOpacity={0.6}>
          <Image
            style={styles.closeBtn}
            source={item.icon}
            resizeMode={'cover'} />
        </TouchableOpacity>
      );
    });
  }

  _renderBody(): Element<*> {
    return (
      <ScrollView
        ref={(scrollview: *) => { this._pager = scrollview; }}
        style={styles.contentContainer}
        horizontal
        pagingEnabled
        scrollEnabled={false}
        keyboardShouldPersistTaps={'handled'}>
        {this._renderRegularPost()}
        {this._renderReactionPost()}
      </ScrollView>
    );
  }

  _renderRegularPost(): Element<*> {
    return (
      <View style={styles.regularPostContainer}>
        <TextInput
          ref={(input: *) => { this._regularInput = input; }}
          style={styles.input}
          selectionColor={COLORS.secondary}
          multiline
          autoFocus
          keyboardType={'twitter'}
          onChangeText={this._onRegularPostChangeText}
          value={this.state.comment}
          placeholder={"What's up?"}
          placeholderTextColor={COLORS.stone} />
      </View>
    );
  }

  _renderPostBtn(): Element<*> {
    const backgroundColor = this._canPost() ? COLORS.primary : COLORS.stone;

    return (
      <KeyboardAvoidingView behavior={'position'}>
        <TouchableHighlight
          style={[styles.postBtn, { backgroundColor }]}
          onPress={this._onPost}
          underlayColor={COLORS.primaryDark}
          disabled={!this._canPost()}>
          <Text style={STYLES.smWhiteText}>POST</Text>
        </TouchableHighlight>
      </KeyboardAvoidingView>
    );
  }

  _renderReactionPost(): Element<*> {
    return (
      <View style={styles.regularPostContainer}>
        <View style={styles.emojiListContainer}>
          {this._renderEmojiTypes()}
        </View>
        <View style={styles.reactionPreview}>
          <Text style={styles.reactionEmoji}>{this.state.reactionIcon}</Text>
          <Text style={STYLES.lgText}>
            {'Eylam Fried is'}
          </Text>
          <TextInput
            ref={(input: *) => { this._reactionInput = input; }}
            style={styles.reactionInput}
            placeholder={this.state.reaction.label}
            placeholderTextColor={COLORS.stone}
            autoCapitalize={'none'}
            selectionColor={COLORS.primary}
            selectTextOnFocus
            maxLength={20}
            onChangeText={this._onReactionLabelChange}
            value={this.state.reactionLabel} />
        </View>
      </View>
    );
  }

  _renderEmojiTypes(): Array<Element<*>> {
    return EMOJI_REACTIONS.map((emoji: Object, index: number): Element<*> => {
      const backgroundColor = this.state.reactionIcon == emoji.icon ?
        convertToOpacity(COLORS.primaryDark, 0.4) : COLORS.white;

      return (
        <TouchableOpacity
          key={index}
          style={[styles.emojiReactionBtn, { backgroundColor }]}
          onPress={this._selectReaction.bind(this, emoji)}
          activeOpacity={0.6}>
          <Text style={STYLES.text}>{emoji.icon}</Text>
        </TouchableOpacity>
      );
    });
  }

  render(): Node {
    return (
      <StatusBarView statusBarColor={COLORS.white}>
        {this._renderHeader()}
        {this._renderBody()}
        {this._renderPostBtn()}
      </StatusBarView>
    );
  }
}

// add user info as props using connect

export default connect()(CreateFeedPost);