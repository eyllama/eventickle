// @flow

import { StyleSheet } from 'react-native';
import { STYLES, COLORS } from '../../Styles';
import Device from '../../utils/Device';

export default StyleSheet.create({
  header: {
    paddingHorizontal: 24,
    paddingVertical: 12,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.smoke,
    ...STYLES.row,
  },

  closeBtn: {
    width: 24,
    height: 24,
  },

  titleText: {
    flex: 1,
    marginLeft: 24,
    ...STYLES.text,
    ...STYLES.darkText,
    ...STYLES.boldText,
  },

  postTypeBtn: {
    width: 24,
    height: 24,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: COLORS.stone,
    marginLeft: 24,
    ...STYLES.center,
  },

  contentContainer: {
    width: Device.screenWidth * 3,
    ...STYLES.whiteContainer,
  },

  regularPostContainer: {
    width: Device.screenWidth,
    paddingHorizontal: 24,
    paddingVertical: 12,
  },

  input: {
    minHeight: 100,
    ...STYLES.text,
  },

  postBtn: {
    height: 50,
    backgroundColor: COLORS.primary,
    ...STYLES.center,
  },

  // Reaction

  emojiListContainer: {
    paddingTop: 12,
    paddingBottom: 24,
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: COLORS.smoke,
    ...STYLES.row,
  },

  emojiReactionBtn: {
    width: 40,
    height: 40,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: COLORS.smoke,
    ...STYLES.center,
  },

  reactionPreview: {
    paddingVertical: 24,
    ...STYLES.row,
  },

  reactionEmoji: {
    marginRight: 12,
    paddingBottom: 4,
    ...STYLES.lgText,
  },

  reactionInput: {
    flex: 1,
    minWidth: 100,
    paddingVertical: 2,
    paddingHorizontal: 6,
    ...STYLES.lgText,
    ...STYLES.boldText,
  },

});