// @flow

import React from 'react';
import type { Node, Element } from 'react';
import { connect } from 'react-redux';

import {
  View,
  ScrollView,
  Animated,
  Image,
  Text,
  TouchableHighlight,
  FlatList,
} from 'react-native';
import { STYLES, COLORS, convertToOpacity } from '../../Styles';
import styles from './styles';
import Event from '../../models/Event';
import FeedPost from '../../models/FeedPost';
import ImageView from '../../components/utils/ImageView';
import Optional from '../../components/utils/Optional';
import Device from '../../utils/Device';
import GlobalController from '../../utils/GlobalController';
import { Navigation } from 'react-native-navigation';
import AttendanceModal from '../../components/modals/AttendanceModal';
import FeedPostItem from '../../components/lists/items/FeedPostItem';
import * as FeedActions from '../../redux/actions/FeedActions';
import PagerBar from '../../components/utils/PagerBar';

type Props = {
  item: Event,
  feed: Array<FeedPost>,
  dispatch: (any) => void,
  componentId: string,
};

type State = {
  scrollX: Animated.Value,
};

class EventDetails extends React.PureComponent<Props, State> {

  _pager: * = null;

  state = {
    scrollX: new Animated.Value(0),
  };

  async componentDidMount() {
    try {
      await this.props.dispatch(FeedActions.fetchFeedForEvent(this.props.item.eid));
    } catch (e) {

    }
  }

  _showUpdateAttendanceModal = () => {
    const {
      event_name,
      host_id,
      host_first_name,
      host_avatar,
      attendance_status
    } = this.props.item;

    const childProps = { event_name, host_id, host_first_name, host_avatar: 'https://scontent-sjc3-1.xx.fbcdn.net/v/t1.0-9/13442371_10206882141507216_3960960229936601581_n.jpg?_nc_cat=105&oh=dea23472734f866c4ae9f207fd080636&oe=5C504DF2', attendance_status };

    const component = (
      <AttendanceModal {...childProps} />
    );

    GlobalController.showModal('slide', component);
  }

  _onRemovePress = () => {
    GlobalController.showErrorModal({
      title: '✋',
      titleStyle: { fontSize: 28 },
      message: 'Are you sure you want to remove this event?',
      buttonTitle: 'YES',
      buttonStyle: {
        backgroundColor: COLORS.red,
      },
    });
  }

  _feedKeyExtractor = (item: Object, index: number): string => item.pid.toString();

  _scrollPager = (index: number) => {
    this._pager._component.scrollTo({
      x: Device.screenWidth * index, y: 0, animated: true
    });
  }

  _goToCreatePost = () => {
    Navigation.showModal({
      component: {
        name: 'CreateFeedPost',
      },
    });
  }

  _goToPost = (post: FeedPost) => {
    Navigation.push(this.props.componentId, {
      component: {
        name: 'FeedPostDetails',
        passProps: {
          post,
        },
      },
    });
  }

  _close = () => {
    // Navigation.pop(this.props.componentId);
    Navigation.dismissModal(this.props.componentId);
  }

  _renderCoverPhoto(): Element<*> {
    return (
      <ImageView
        style={styles.coverPhoto}
        source={this.props.item.coverPhoto}
        resizeMode={'cover'}>
        <TouchableHighlight
          onPress={this._close}>
          <Text style={STYLES.whiteText}>CLOSE</Text>
        </TouchableHighlight>
      </ImageView>
    );
  }

  _renderPager(): Element<*> {
    return (
      <PagerBar
        pages={['Info', 'Feed']}
        animatedValue={this.state.scrollX}
        onTabPress={this._scrollPager}
        selectorColor={COLORS.stone} />
    );
  }

  _renderInfo(): Element<*> {
    return (
      <ScrollView
        style={styles.infoContainer}
        contentContainerStyle={{ paddingTop: 24 }}>
        {this._renderHostInfo()}
        <View style={{ paddingHorizontal: 24 }}>
          <Text style={styles.titleText}>{this.props.item.event_name}</Text>
          <Text style={STYLES.grayText}>{this.props.item.event_description}</Text>
        </View>
        {this._renderDetails()}
        {this._renderRemoveBtn()}
      </ScrollView>
    );
  }

  _renderHostInfo(): Element<*> {
    const { rsvpColor } = this.props.item;

    return (
      <View style={[styles.hostInfoContainer, { borderBottomColor: rsvpColor }]}>
        <View style={[STYLES.row, { marginBottom: 24 }]}>
          <Image
            style={styles.hostAvatar}
            source={{ uri: 'https://scontent-sjc3-1.xx.fbcdn.net/v/t1.0-9/13442371_10206882141507216_3960960229936601581_n.jpg?_nc_cat=105&oh=dea23472734f866c4ae9f207fd080636&oe=5C504DF2' }}
            resizeMode={'cover'} />
          <Text style={styles.hostsEventTitle}>
            {(`${this.props.item.host_first_name}'s event`).toUpperCase()}
          </Text>
        </View>
        <View style={STYLES.row}>
          <View style={[STYLES.row, STYLES.flexOne]}>
            <Text style={[STYLES.smLightText, STYLES.boldText, { marginRight: 12 }]}>{'Attendance:'}</Text>
            <Text style={STYLES.smLightText}>
              {this.props.item.attendanceStatus}
            </Text>
            <View style={[styles.attendanceStatusIcon, { backgroundColor: rsvpColor }]} />
          </View>
          <TouchableHighlight
            style={styles.attendanceBtn}
            onPress={this._showUpdateAttendanceModal}
            underlayColor={COLORS.stone}>
            <Text style={STYLES.xsText}>UPDATE</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  _renderDetails(): Element<*> {
    return (
      <View style={styles.detailsContainer}>

      </View>
    );
  }

  _renderRemoveBtn(): Element<*> {
    return (
      <TouchableHighlight
        style={[
          styles.bottomBtn,
          { backgroundColor: convertToOpacity(COLORS.red, 0.8) }
        ]}
        onPress={this._onRemovePress}
        underlayColor={COLORS.red}>
        <Text style={[STYLES.xsWhiteText, STYLES.boldText]}>REMOVE</Text>
      </TouchableHighlight>
    );
  }

  // Feed

  _renderFeed(): Element<*> {
    return (
      <View style={styles.feedContainer}>
        <FlatList
          style={styles.feedList}
          contentContainerStyle={styles.feedContentContainer}         
          data={this.props.feed}
          keyExtractor={this._feedKeyExtractor}
          renderItem={this._renderFeedItem} />
        <TouchableHighlight
          style={[
            styles.bottomBtn,
            { backgroundColor: convertToOpacity(COLORS.primary, 0.8) }
          ]}
          onPress={this._goToCreatePost}
          underlayColor={COLORS.primaryDark}>
          <Text style={[STYLES.xsWhiteText, STYLES.boldText]}>WRITE A POST</Text>
        </TouchableHighlight>
      </View>
    );
  }

  _renderFeedItem = ({ item }: Object): Element<*> => {
    const disabled = item.type == 'reaction';

    return (
      <FeedPostItem
        post={item}
        goToPost={this._goToPost}
        disabled={disabled} />
    );
  }
  
  render(): ?Node {
    if (!this.props.item) {
      return null;
    }

    return (
      <View style={STYLES.whiteContainer}>
        {this._renderCoverPhoto()}
        {this._renderPager()}
        <Animated.ScrollView
          ref={(scrollview: *) => { this._pager = scrollview; }}
          horizontal
          pagingEnabled
          contentContainerStyle={styles.pagerContentContainer}
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: this.state.scrollX } } }],
            { useNativeDriver: true },
          )}>
          {this._renderInfo()}
          {this._renderFeed()}
        </Animated.ScrollView>
      </View>
    );
  }

}

const mapStateToProps = (state: Object) => {
  return {
    feed: state.feed,
  };
}

export default connect(mapStateToProps)(EventDetails);