// @flow

import { StyleSheet } from 'react-native';
import { STYLES, COLORS, convertToOpacity } from '../../Styles';
import Device from '../../utils/Device';

export default StyleSheet.create({
  coverPhoto: {
    height: 180,
    justifyContent: 'flex-end',
    paddingBottom: 24,
    paddingHorizontal: 24,
  },

  pagerContentContainer: {
    width: Device.screenWidth * 2,
  },

  bottomBtn: { 
    height: 32,
    paddingHorizontal: 32,
    marginVertical: 18,
    borderRadius: 16,
    alignSelf: 'center',
    ...STYLES.center,
  },

  // Info

  infoContainer: {
    width: Device.screenWidth,
  },

  hostInfoContainer: {
    marginBottom: 24,
    marginHorizontal: 24,
    paddingBottom: 24,
    borderBottomWidth: 1,
  },

  hostsEventTitle: {
    marginLeft: 12,
    ...STYLES.smText,
    color: COLORS.primary,
  },

  hostAvatar: {
    width: 24,
    height: 24,
    borderRadius: 12,
  },

  titleText: {
    marginBottom: 12,
    ...STYLES.xlText,
  },

  attendanceStatusIcon: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: COLORS.green,
    marginLeft: 12,
  },

  attendanceBtn: {
    width: 80,
    height: 24,
    borderWidth: 1,
    borderColor: COLORS.stone,
    borderRadius: 12,
    backgroundColor: 'transparent',
    paddingTop: 2,
    ...STYLES.center,
  },

  detailsContainer: {
    marginTop: 24,
    backgroundColor: COLORS.cotton,
    height: 160,
  },

  // feed

  feedContainer: {
    width: Device.screenWidth,
  },

  feedList: {
    backgroundColor: COLORS.cotton,
    ...STYLES.flexOne,
    ...STYLES.tabHairline,
  },

  feedContentContainer: {
    paddingTop: 6,
  },


  // pager

  pager: {
    height: 50,
    ...STYLES.tabHairline,
  },

});