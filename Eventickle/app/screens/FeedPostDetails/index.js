// @flow

import React from 'react';
import type { Element, Node } from 'react';

import {
  View,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import { STYLES, COLORS } from '../../Styles';
import styles from './styles';
import FeedPostItem from '../../components/lists/items/FeedPostItem';
import FeedPost from '../../models/FeedPost';
import { Navigation } from 'react-native-navigation';
import SimpleHeader from '../../components/headers/SimpleHeader';
import StatusBarView from '../../components/utils/StatusBarView';

type Props = {
  post: FeedPost,
  componentId: string,
};

class FeedPostDetails extends React.PureComponent<Props> {

  _commentsList: * = null;

  _close = () => {
    Navigation.pop(this.props.componentId);
  }

  _renderHeader(): Element<*> {
    return (
      <SimpleHeader
        statusBarColor={COLORS.cotton}
        onBack={this._close} />
    );
  }

  _renderFeedPostComment = ({ item }: Object): Element<*> => {
    return (
      <View />
    );
  }

  render(): ?Node {
    if (!this.props.post) {
      return null;
    }

    return (
      <View style={STYLES.whiteContainer}>
        {this._renderHeader()}
        <FeedPostItem post={this.props.post} disabled />
        <FlatList
          ref={(list: *) => { this._commentsList = list; }}
          data={[]}
          renderItem={this._renderFeedPostComment} />
      </View>
    );
  }
}

export default FeedPostDetails;