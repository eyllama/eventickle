// @flow

import { StyleSheet } from 'react-native';
import { STYLES, COLORS } from '../../Styles';

export default StyleSheet.create({
  header: {
    backgroundColor: COLORS.primary,
  },
});