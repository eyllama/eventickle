// @flow

import { StyleSheet } from 'react-native';
import { STYLES, COLORS } from '../../../Styles';

export default StyleSheet.create({
  headerRight: {
    height: 32,
    borderRadius: 16,
    paddingHorizontal: 24,
    backgroundColor: COLORS.primaryDark,
    borderWidth: 1, //StyleSheet.hairlineWidth,
    borderColor: COLORS.primaryLight,
    ...STYLES.center,
  }
});