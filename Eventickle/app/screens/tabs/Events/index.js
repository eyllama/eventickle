// @flow

import React from 'react';
import type { Element, Node } from 'react';
import { connect } from 'react-redux';

import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableHighlight,
} from 'react-native';
import { STYLES, COLORS } from '../../../Styles';
import styles from './styles';
import Event from '../../../models/Event';
import EventListItem from '../../../components/lists/items/EventListItem';
import * as EventActions from '../../../redux/actions/EventActions';
import { Navigation } from 'react-native-navigation';
import LargeHeader from '../../../components/headers/LargeHeader';

type Props = {
  events: Array<Event>,
  dispatch: (any) => void,
  componentId: string,
};

type State = {

};

class Events extends React.PureComponent<Props, State> {

  _eventsList: *;

  state = { 
  };

  async componentDidMount() {
    try {
      await this.props.dispatch(EventActions.fetchAllEventsForUser(1));
    } catch (e) {
      console.warn('Error getting Events in Events', e);
    }
  }

  _goToEvent = (eventItem: Event) => {
    Navigation.showModal({
      stack: {
        children: [
          { component: { name: 'FeedPostDetails', } },
          {
            component: {
            name: 'EventDetails',
            passProps: { item: eventItem },
          }
        }]
      }
    });
  }

  _goToCreateEvent = () => {
    Navigation.showModal({
      component: {
        name: 'CreateEvent',
      }
    });
  }

  _renderLoadingScreen(): Element<*> {
    return (
      <View style={[STYLES.whiteContainer, STYLES.center]}>
        <ActivityIndicator size={'small'} color={COLORS.primary} />
      </View>
    );
  }

  _renderHeaderCreate(): Element<*> {
    return (
      <TouchableHighlight
        style={styles.headerRight}
        onPress={this._goToCreateEvent}
        underlayColor={COLORS.primaryLight}>
        <Text style={[STYLES.xsWhiteText, STYLES.boldText]}>CREATE</Text>
      </TouchableHighlight>
    );
  }

  _keyExtractor = (item, index) => '' + index;

  _renderItem = ({ item }: Object): Element<*> => {
    return (
      <EventListItem item={item} goToEvent={this._goToEvent} />
    );
  }

  render(): Node {
    if (this.state.loading) {
      return this._renderLoadingScreen();
    } else {
      return (
      <View style={[STYLES.whiteContainer, STYLES.tabHairline]}>
        <LargeHeader
          title={'Events'}
          titleStyle={[STYLES.flexOne, { color: COLORS.white }]}
          backgroundColor={COLORS.primary}
          statusBarColor={COLORS.primary}
          hasShadow
          rightComponent={this._renderHeaderCreate()} />
        <FlatList
          ref={(ref: *) => { this._eventsList = ref; }}
          keyExtractor={this._keyExtractor}
          data={this.props.events}
          renderItem={this._renderItem} />
      </View>
    );
    }
  }
}

const mapStateToProps = (state: Object) => {
  return {
    events: state.events,
  };
}

export default connect(mapStateToProps)(Events);