// @flow

import { Navigation } from 'react-native-navigation';

import Events from './screens/tabs/Events';
import EventDetails from './screens/EventDetails';
import CreateFeedPost from './screens/CreateFeedPost';
import FeedPostDetails from './screens/FeedPostDetails';
import CreateEvent from './screens/CreateEvent';

import SlideModal from './components/modals/SlideModal';
import PopupModal from './components/modals/PopupModal';

import SimpleHeader from './components/headers/SimpleHeader';

export function registerScreens(store: Object, Provider: *): void {
  // screens
  Navigation.registerComponentWithRedux('Events', () => Events, Provider, store);
  Navigation.registerComponentWithRedux('EventDetails', () => EventDetails, Provider, store);
  Navigation.registerComponentWithRedux('CreateFeedPost', () => CreateFeedPost, Provider, store);
  Navigation.registerComponentWithRedux('FeedPostDetails', () => FeedPostDetails, Provider, store);
  Navigation.registerComponent('CreateEvent', () => CreateEvent);

  // modals
  Navigation.registerComponent('SlideModal', () => SlideModal);
  Navigation.registerComponent('PopupModal', () => PopupModal);

  // headers
  Navigation.registerComponent('SimpleHeader', () => SimpleHeader);
}