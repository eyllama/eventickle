const pg = require('pg');

const pool = new pg.Pool({
  host: 'localhost',
  user: 'eylamfried',
  database: 'eventickle',
  max: 10,
});

module.exports = pool;