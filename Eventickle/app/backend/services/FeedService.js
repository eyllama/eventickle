module.exports = function(app) {
  const pool = require('../Connection');
  
  app.get('/api/event/:eventID/feed', findFeedForEvent);
  app.post('/api/event/feed', createFeedPost);

  function findFeedForEvent(req, res) {
    const { eventID } = req.params;

    if (!eventID) {
      res.sendStatus(400);
      return;
    }

    pool.connect((err, db, release) => {
      if (err) {
        console.error('Could not connect in - findFeedForEvent', err);
        res.sendStatus(500);
        return;
      }

      db.query(`
        SELECT p.*, u.username, u.first_name, u.last_name, u.profile_picture
        FROM Post AS p INNER JOIN "User" AS u
        ON p.uid = u.uid
        WHERE p.eid = ${eventID}
      `, (err, result) => {
        release();

        if (err) {
          console.error('Error getting event after query - findFeedForEvent', err);
          res.sendStatus(404);
          return;
        }

        res.json(result.rows);
      });
    });
  }

  function createFeedPost(req, res) {
    const post = req.body;

    if (!post) {
      res.sendStatus(400);
      return;
    }

    console.log(post);

    pool.connect((err, db, release) => {
      if (err) {
        console.error('Could not connect in - createFeedPost', err);
        res.sendStatus(500);
        return;
      }

      let query = '';      

      if (post.type == 'regular') {
        query = `INSERT INTO Post (eid, uid, type, comment, date_created) VALUES (
          ${post.eid}, ${post.uid}, '${post.type}', '${post.comment}', NOW()
        ) RETURNING *`;
      } else if (post.type == 'reaction') {
        query = `INSERT INTO Post (eid, uid, type, reaction_icon, reaction_label, date_created) VALUES (
          ${post.eid}, ${post.uid}, '${post.type}', '${post.reaction_icon}', '${post.reaction_label}', NOW()
          ) RETURNING *`;
        console.log(query);
      }

      db.query(query, (err, result) => {
        release();

        if (err) {
          console.error('Error creating post after query - createFeedPost', err);
          res.sendStatus(404);
          return;
        }

        const { username, first_name, last_name, profile_picture } = post;

        res.json({
          ...result.rows[0],
          username,
          first_name,
          last_name,
          profile_picture,
        });
      });
    });

  }
};