module.exports = function(app) {
  const pool = require('../Connection');

  app.get('/api/event/:eventID', findEventByID);
  app.post('/api/user/event', findAllEventsForUser);

  function findEventByID(req, res) {
    const { eventID } = req.params;

    if (!eventID) {
      res.sendStatus(400);
      return;
    }

    pool.connect((err, db, release) => {
      if (err) {
        console.error('Could not connect in - findEventByID', err);
        res.sendStatus(500);
        return;
      }

      db.query(`SELECT * FROM "Event" WHERE eid = ${eventID}`, (err, result) => {
        release();

        if (err) {
          console.error('Error getting event after query - findEventByID', err);
          res.sendStatus(404);
          return;
        }

        res.json(result.rows);
      });
    });
  }

  function findAllEventsForUser(req, res) {
    const userID = req.body.userID;

    if (!userID) {
      console.error('Could not get userID in - findAllEventsForUser', err);
      res.sendStatus(400);
      return;
    }

    pool.connect((err, db, release) => {
      if (err) {
        console.error('Could not connect in - findAllEventsForUser', err);
        res.sendStatus(500);
        return;
      }

      db.query(`
        SELECT ue.attendance_status, e.eid, e.name AS event_name, e.description AS event_description,
        e.cover_photo, e.host_id, e.start_time, e.date_occurring, u.profile_picture AS host_avatar,
        u.first_name AS host_first_name, a.*
        FROM "User" u, User_Event ue, "Event" e, Address a
        WHERE ue.eid = e.eid AND u.uid = e.host_id AND e.aid = a.aid
      `, (err, result) => {
        release();

        if (err) {
          console.error('Error getting events for user - findAllEventsForUser', err);
          res.sendStatus(404);
          return;
        }

        res.json(result.rows);
      });
    });
  }
};