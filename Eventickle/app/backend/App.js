module.exports = function(app) {
  require('./services/EventService')(app);
  require('./services/FeedService')(app);
};